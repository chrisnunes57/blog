import React, {Component} from 'react';
import './about.css';
import Selfie from "../../media/selfie.JPG";



class About extends Component {

    render() {
      return (
          <div className="container about">
              <div className="right">
                <img src={Selfie} alt={"It's a picture of Chris"} />
              </div>
              <div className="left">
                  <h1>Hey! I'm Chris.</h1>
                  <p>I am a freelance web developer, and a student at the University of Texas at Austin.
                      I plan on graduating in Spring of 2021 with a degree in Computer Science.</p>
                  <p>I really enjoy creating cool effects online with Javascript and CSS, and coding interactions that
                    are as accessible as they are cool.</p>
                  <p>When I'm not at my computer, I really enjoy staying active, either by playing sports or being in the gym.
                    Most of my free time is occupied with basketball and rock climbing. </p>
                  <h3 className="pls-hire-me">Lets work together!</h3>
                  <h>Contact me at <a className="contact-email" href="mailto:chrisnunes57@gmail.com">chrisnunes57@gmail.com</a>.</h>
              </div>
          </div>
      );
  }
}

export default About;
