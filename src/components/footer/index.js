import React, {Component} from 'react';
import './footer.css';
import FooterCategoryList from "./FooterCategoryList";



class Footer extends Component {

    render() {
      return (
         <div className="footer">
            <div className="container footer-container">
                <h1 className="footer-title">chris nunes</h1>
                <hr className="separator"/>
                <p className="footer-subtitle">Sections</p>
                <FooterCategoryList />
            </div>
         </div>
      );
  }
}

export default Footer;
