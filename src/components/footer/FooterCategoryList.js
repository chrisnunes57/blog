import React, {Component} from 'react';
import './footer.css';
import {Link} from "react-router-dom";

class FooterCategoryList extends Component {

    render() {
      return (
         <div className="footer-category-list">
             <Link className="footer-list-item" to="/section/travel">
                 <p>Travel</p>
             </Link>
             <Link className="footer-list-item" to="/section/web-development">
                 <p>Web Development</p>
             </Link>
         </div>
      );
  }
}

export default FooterCategoryList;
