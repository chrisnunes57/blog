import React, {Component, Fragment} from 'react';
import './homePage.css';
import HeroPost from "../heroPost";
import FeaturedPosts from "../featuredPosts";



class HomePage extends Component {

    render() {
      return (
         <Fragment>
             <HeroPost />
             <FeaturedPosts />
         </Fragment>
      );
  }
}

export default HomePage;
