import React, {Component} from 'react';
import './notFound404.css';



class NotFound404 extends Component {

    render() {
      return (
          <div className="container not-found">
              <h1>Error 404: We couldn't find the page you requested</h1>
          </div>
      );
  }
}

export default NotFound404;
