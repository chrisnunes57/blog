import React, {Component, Fragment} from 'react';
import './postList.css';
import FeaturedCard from "../featuredPosts/featuredCard";
import StockholmImage from "../../media/stockholm.jpeg";
import PolyShoeImage from "../../media/poly-shoe.png";
import DragginImage from "../../media/dragginjs_screenshot.png";
import CSSImage from "../../media/css_art.png";
import NavbarImage from "../../media/nice-navbar.png";



class WebDev extends Component {

    render() {
        return (
            <section className="container">
                <h1 className="page-title">{this.props.title}</h1>
                <div className="grid">
                    <FeaturedCard
                        size={4}
                        imgSrc={PolyShoeImage}
                        postTitle={"The Intersection of Sneakers and Javascript"}
                        postAuthor={"Chris Nunes"}
                        authorLink={"/author/chris-nunes"}
                        postLink={"/posts/sneaker-art"}
                    />
                    <FeaturedCard
                        size={4}
                        imgSrc={NavbarImage}
                        postTitle={"Recreating a Really Cool Navbar Animation"}
                        postAuthor={"Chris Nunes"}
                        authorLink={"/author/chris-nunes"}
                        postLink={"/posts/navbar-design"}
                    />
                    <FeaturedCard
                        size={4}
                        imgSrc={CSSImage}
                        postTitle={"Creating Art with Nothing But CSS"}
                        postAuthor={"Chris Nunes"}
                        authorLink={"/author/chris-nunes"}
                        postLink={"/posts/css-art"}
                    />
                </div>
            </section>
        );
  }
}

export default WebDev;
