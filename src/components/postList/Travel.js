import React, {Component, Fragment} from 'react';
import './postList.css';
import FeaturedCard from "../featuredPosts/featuredCard";
import StockholmImage from "../../media/stockholm.jpeg";
import PolyShoeImage from "../../media/poly-shoe.png";
import DragginImage from "../../media/dragginjs_screenshot.png";
import CSSImage from "../../media/css_art.png";



class Travel extends Component {

    render() {
        return (
            <section className="container">
                <h1 className="page-title">{this.props.title}</h1>
                <div className="grid">
                    <FeaturedCard
                        size={4}
                        imgSrc={StockholmImage}
                        postTitle={"Studying Abroad in Sweden"}
                        postAuthor={"Chris Nunes"}
                        authorLink={"/author/chris-nunes"}
                        postLink={"/posts/sweden-reflections"}
                    />
                </div>
            </section>
        );
  }
}

export default Travel;
