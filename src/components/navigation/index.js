import React from 'react';
import './navigation.css';
import {Link} from "react-router-dom";

function Navigation() {
  return (
    <nav className="nav-wrapper">
        <div className="nav container">
            <Link to="/" className={"nav-left"}>
                <h1 className="logo-text">Chris Nunes</h1>
            </Link>
            <div className="nav-right">
                <Link to="/" className={"nav-item"}>
                    <p tabIndex={0}>Home</p>
                </Link>
                <Link to="/posts" className={"nav-item"}>
                    <p tabIndex={0}>Latest</p>
                </Link>
                <Link to="/about" className={"nav-item"}>
                    <p tabIndex={0}>About</p>
                </Link>
            </div>
        </div>
    </nav>
  );
}

export default Navigation;
