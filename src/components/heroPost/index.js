import React, {Component} from 'react';
import './heroPost.css';
import Parallax from 'react-rellax';
import bg from "../../media/bg.png";
import bg_1 from "../../media/bg-1.png";
import bg_2 from "../../media/bg-2.png";
import eye from "../../media/eye.png";
import half_copy from "../../media/half-copy.png";
import left from "../../media/left.png";
import neck from "../../media/neck.png";
import shirt from "../../media/shirt.png";
import {Link} from "react-router-dom";



class HeroPost extends Component {

    render() {
      return (
          <section className="hero-wrapper">
              <div className="container hero-container">
                  <div className="hero-image-wrapper">
                      {/*<img className="hero-image" src={"https://images.unsplash.com/photo-1497217968520-7d8d60b7bc25?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2100&q=80"} />*/}
                      <div className="grid-container" aria-hidden="true">
                          <div className="grid-hero" aria-hidden="true">
                              <div className="grid__item grid__item--bg">
                                  <img src={bg} alt="" />
                              </div>
                              <Parallax speed={3} className="grid__item grid__item--portrait-half rellax">
                                  <img
                                      src={half_copy} alt="" />
                              </Parallax>
                              <Parallax  speed={-1} className="grid__item grid__item--portrait-neck rellax">
                                  <img src={neck} alt="" />
                              </Parallax>
                              <Parallax  speed={5} className="grid__item grid__item--portrait-left rellax">
                                  <img
                                      src={left} alt="" />
                              </Parallax>
                              <Parallax  speed={1} className="grid__item grid__item--portrait-eye rellax">
                                  <img src={eye} alt="" />
                              </Parallax>
                              <Parallax  speed={4} className="grid__item grid__item--portrait-shirt rellax">
                                  <img src={shirt} alt="" />
                              </Parallax>
                              <Parallax  speed={-2} className="grid__item grid__item--portrait-bg-1 rellax">
                                  <img src={bg_1} alt="" />
                              </Parallax>
                              <Parallax  speed={3} className="grid__item grid__item--portrait-bg-2 rellax">
                                  <img src={bg_2} alt="" />
                              </Parallax>
                              <Parallax  speed={3} className="grid__item grid__item--portrait-bg-3 rellax">
                                  <img src={bg_1} alt="" />
                              </Parallax>
                              <Parallax  speed={-2} className="grid__item grid__item--portrait-bg-4 rellax">
                                  <img src={bg_2} alt="" />
                              </Parallax>
                          </div>
                      </div>
                  </div>
                  <div className="hero-content">
                      <Link to={"section/travel"} className="post-category">
                          Travel
                      </Link>
                      <Link to={"/posts/sweden-reflections"}>
                          <h1 className="post-title">Studying Abroad in Sweden</h1>
                          <p className="post-description">I want to write about my experiences studying in Stockholm, and how I feel now that my time abroad was cut short.</p>
                      </Link>
                      <Link to="/author/chris-nunes" className="post-author">
                          By Chris Nunes
                      </Link>
                  </div>
              </div>
          </section>
      );
  }
}

export default HeroPost;
