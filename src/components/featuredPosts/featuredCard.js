import React, {Component} from 'react';
import './featuredPosts.css';
import {Link} from "react-router-dom";



class FeaturedCard extends Component {

    render() {
      return (
          <div className={"featured-card column-" + this.props.size}>
              <Link to={this.props.postLink}>
                  <div className="featured-card-image" style={{backgroundImage: "url('"+this.props.imgSrc+"')"}} />
              </Link>
              <Link to={"section/web-development"} className="post-category">
                  {this.props.category}
              </Link>
              <Link to={this.props.postLink}>
                  <h1 className="post-title">{this.props.postTitle}</h1>
              </Link>
              <Link to={this.props.authorLink} className="post-author">
                  By {this.props.postAuthor}
              </Link>
          </div>
      );
  }
}

export default FeaturedCard;
