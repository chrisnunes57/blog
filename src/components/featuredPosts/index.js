import React, {Component} from 'react';
import './featuredPosts.css';
import FeaturedCard from "./featuredCard";
import {Link} from "react-router-dom";
import NavbarImage from "../../media/nice-navbar.png";
import CSSImage from "../../media/css_art.png";
import PolyShoeImage from "../../media/poly-shoe.png";



class FeaturedPosts extends Component {

    render() {
      return (
          <section className="featured-posts container">
              <a href="#" className="post-category featured-posts-title">Featured Posts</a>
              <div className="grid">
                  <FeaturedCard
                      size={4}
                      imgSrc={PolyShoeImage}
                      postTitle={"The Intersection of Sneakers and Javascript"}
                      postAuthor={"Chris Nunes"}
                      authorLink={"/author/chris-nunes"}
                      postLink={"/posts/sneaker-art"}
                  />
                  <FeaturedCard
                      size={4}
                      imgSrc={NavbarImage}
                      postTitle={"Recreating a Really Cool Navbar Animation"}
                      postAuthor={"Chris Nunes"}
                      authorLink={"/author/chris-nunes"}
                      postLink={"/posts/navbar-design"}
                  />
                  <FeaturedCard
                      size={4}
                      imgSrc={CSSImage}
                      postTitle={"Creating Art with Nothing But CSS"}
                      postAuthor={"Chris Nunes"}
                      authorLink={"/author/chris-nunes"}
                      postLink={"/posts/css-art"}
                  />
              </div>
              <Link to="/posts">
                  <button className="button all-posts">View all latest posts</button>
              </Link>
          </section>
      );
  }
}

export default FeaturedPosts;
