import React, {Component, Fragment} from 'react';
import './postView.css';
import SalmonToeImage from "../../media/salmon-toes.jpg";
import VapormaxImage from "../../media/vapormax.jpg";
import BigPolyShoeImage from "../../media/poly-shoe-big.png";
import SneakerGif from "../../media/sneaker-gif.gif";


class PolyShoePost extends Component {

    render() {
      return (
          <Fragment>
              <div className="container post-view">
                  <h1 className={"page-title"}>The Intersection of Sneakers and Javascript</h1>
                  <h3>March 23, 2020</h3>
                  <div className="text-container">
                      <p>Ever since my junior year of high school, I have been obsessed with sneakers. I don't remember when or how exactly
                        it happened, but somehow I ended up with more pairs of shoes than I can keep track of.</p>
                      <p>Since then, I've become more financially savvy and stopped buying as many shoes. However, sneakers are still
                      a passion/hobby of mine, so whenever I want to test out a new idea, I try to make it sneaker-related.</p>
                      <div className="wide-image-wrapper">
                          <img src={SalmonToeImage} alt={"A blue sneaker with a distinctive pink color on the toe"}/>
                      </div>
                  </div>
              </div>
              <div className="container post-view">
                  <div className="text-container">
                      <p>This bad boy right here is a&nbsp;
                          <a href={"https://stockx.com/asics-gel-lyte-iii-ronnie-fieg-salmon-toes"}>Ronnie Fieg x Asics Gel-Lyte III "Salmon Toes"</a>
                        &nbsp;sneaker. These are a pretty famous Asics collaboration, so when I decided to mess around and learn SVG line animations,
                        this sneaker was the first model that came to mind.</p>
                      <p>In this next CodePen, scroll down inside of the project and watch the shoe animate itself into existence.</p>
                      <iframe height="500" style={{width: "100%"}} scrolling="no" title="Scrolling Shoe SVG Animation"
                              src="https://codepen.io/chrisnunes57/embed/Ldvvbq?height=265&theme-id=light&default-tab=result"
                              frameBorder="no" allowTransparency="true" allowFullScreen="true">
                          See the Pen <a href='https://codepen.io/chrisnunes57/pen/Ldvvbq'>Scrolling Shoe SVG
                          Animation</a> by Chris Nunes
                          (<a href='https://codepen.io/chrisnunes57'>@chrisnunes57</a>) on <a
                          href='https://codepen.io'>CodePen</a>.
                      </iframe>
                      <p>That project was really cool to me because I got to take shoe images and turn them
                        into SVG files that I could use online, which I had never done before.</p>
                      <p>Now that I had one solid sneaker-related project under my belt,
                          I took my newfound image manipulation skills and put them to use on something more advanced.</p>
                      <div className="wide-image-wrapper">
                          <img src={VapormaxImage} alt={"A black sneaker with bright rainbow-colored rubber along the bottom."}/>
                      </div>
                      <p>The spicy shoe pictured above is the <a href={"https://stockx.com/nike-air-vapormax-be-true-2017"}>
                          Nike Vapormax "Be True"</a>. I really love these shoes, so I wanted to do something special for them.</p>
                      <p>Before I even wrote any code for this project, I had to sharpen my Photoshop skills. I wanted to try creating a
                        "low-poly" effect in my image, which basically means composing the full image out of many smaller geometric shapes.
                        In essence, I tried to recreate the profile of the sneaker using only triangles. This might be confusing to visualize
                        (at least it was for me), so I'll just show you.</p>
                      <div className="wide-image-wrapper">
                          <img src={BigPolyShoeImage} alt={"A black sneaker with bright rainbow-colored rubber along the bottom."}/>
                      </div>
                      <p>Although there are definitely some rough edges and places where the shapes don't exact connect, this was miles
                        better than I had hoped for. Although this image on its own was cool, it was definitely not the end goal. I
                        wanted to add some kind of movement or animation for some real "wowza" factor.</p>
                      <p>Around this time, my friend showed me a website called <a href={"http://species-in-pieces.com/"}>Species in Pieces.</a>
                        &nbsp;The animations on this site blew my mind, and I wanted to create something similar to their effect.</p>
                      <div className="wide-image-wrapper">
                          <img src={SneakerGif} alt={"A black sneaker with bright rainbow-colored rubber along the bottom."}/>
                      </div>
                      <p>You can play around with the actual site <a href={"https://poly-shoe.surge.sh"}>here,</a> and
                          view the Github repos for the <a href={"https://github.com/chrisnunes57/SVG-Shoe-Animation"}>first project</a> and
                          the <a href={"https://github.com/chrisnunes57/poly-art"}>second project</a> at these links.</p>
                  </div>
              </div>
          </Fragment>
      );
  }
}

export default PolyShoePost;
