import React, {Component, Fragment} from 'react';
import './postView.css';



class CssArtPost extends Component {

    render() {
      return (
          <Fragment>
              <div className="container post-view">
                  <h1 className={"page-title"}>Creating Art with Nothing But CSS</h1>
                  <h3>March 23, 2020</h3>
                  <div className="text-container">
                      <p>Around a year ago, I was getting really bored with web development. I had been working a lot with React and Node,
                          and I realllly needed a break from Javascript. When I was almost completely lost in&nbsp;
                          <a href={"http://callbackhell.com/"}>callback hell</a>, I found&nbsp;
                          <a href={"https://a.singlediv.com/"}>this</a> website and had my mind blown. A Single Div is a collection
                          of incredible images that people have generated using (you guessed it) one single &lt;div&gt; element.</p>
                      <p>I thought that was the coolest thing ever, and immediately set off to create my own. After several hours of work, I
                          arrived at my masterpiece:</p>
                  </div>
              </div>
              <div className="box">
                  <div className="face"/>
              </div>
              <div className="container post-view">
                  <div className="text-container">
                      <p>Now, I know what you're thinking. "Wow, that's.......... impressive!".</p>
                      <p>Thank you, I think so too. </p>
                      <hr className={"separator"} />
                      <p>Eventually, after more practice, I got better and better at creating these images. I would go onto&nbsp;
                          <a href={"https://dribbble.com/"}>Dribbble.com</a>, find basic 2D drawings, and recreate them in CSS. I won't
                          embed them all on this page, but they are hosted at&nbsp; <a href={"http://divart.surge.sh/"}>divart.surge.sh</a>
                          &nbsp;if you really want to see.
                        </p>
                      <p>I eventually topped off the collection with my favorite one, which is embedded below:</p>
                      <iframe height="600" style={{width: "100%"}} scrolling="no"
                              title="Workspace in 100% CSS #dailycssimages"
                              src="https://codepen.io/chrisnunes57/embed/OzYrzg?height=265&theme-id=light&default-tab=result"
                              frameBorder="no" allowTransparency="true" allowFullScreen="true">
                          See the Pen <a href='https://codepen.io/chrisnunes57/pen/OzYrzg'>Workspace in 100% CSS
                          #dailycssimages</a> by Chris Nunes
                          (<a href='https://codepen.io/chrisnunes57'>@chrisnunes57</a>) on <a
                          href='https://codepen.io'>CodePen</a>.
                      </iframe>
                      <p>Those are just a few of the cool things that I've been able to do with plain CSS! If you're interested in more
                        small projects and half-thought-out ideas, there are plenty of those on&nbsp;
                          <a href={"https://codepen.io/chrisnunes57"}>my Codepen profile!</a></p>
                  </div>
              </div>
          </Fragment>
      );
  }
}

export default CssArtPost;
