import React, {Component, Fragment} from 'react';
import './postView.css';



class NavbarPost extends Component {

    render() {
      return (
          <Fragment>
              <div className="container post-view">
                  <h1 className={"page-title"}>Recreating a Really Cool Navbar Animation</h1>
                  <h3>March 23, 2020</h3>
                  <div className="text-container">
                      <p>I was browsing a list of tech internships last fall when I found a website with a really cool animation
                          in the navigation section of their page. The website was <a href={"https://www.addepar.com/"}>Addepar</a>, though
                        they have redesigned their website since then.</p>
                      <p>I decided that I wanted to learn how to create that effect, so I did! The finished result is below, and you can view it by
                        opening and closing the embedded menu.</p>
                      <iframe height="265" style={{width: "100%"}} scrolling="no" title="Nice Navbar"
                              src="https://codepen.io/chrisnunes57/embed/xyNNRM?height=265&theme-id=light&default-tab=result"
                              frameBorder="no" allowTransparency="true" allowFullScreen="true">
                          See the Pen <a href='https://codepen.io/chrisnunes57/pen/xyNNRM'>Nice Navbar</a> by Chris
                          Nunes
                          (<a href='https://codepen.io/chrisnunes57'>@chrisnunes57</a>) on <a
                          href='https://codepen.io'>CodePen</a>.
                      </iframe>
                      <p>That's all for now on this post. If there's any interest, I'd really enjoy going back and providing a tutorial or
                        more technical writeup on this effect (I'll probably do it anyways).</p>
                  </div>
              </div>
          </Fragment>
      );
  }
}

export default NavbarPost;
