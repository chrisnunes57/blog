import React, {Component, Fragment} from 'react';
import './postView.css';



class SwedenPost extends Component {

    render() {
      return (
          <Fragment>
              <div className="container post-view">
                  <h1 className={"page-title"}>Studying abroad in Sweden</h1>
                  <h3>March 23, 2020</h3>
                  <div className="text-container">
                      <p>Hej hej! This post actually isn't published yet, but keep checking back for when it is!
                          Also, side note, this site is still in development! Be forgiving of any bugs/issues you run into.</p>
                      <p>If you have any comments/suggestions, feel free to send them to me at <a href={"mailto:chrisnunes57@gmail.com"}>chrisnunes57@gmail.com</a>.</p>
                  </div>
              </div>
          </Fragment>
      );
  }
}

export default SwedenPost;
