import React, {Fragment} from 'react';
import './App.css';
import Navigation from './components/navigation';
import HomePage from "./components/homePage";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import NotFound404 from "./components/notFound404";
import Travel from "./components/postList/Travel";
import WebDev from "./components/postList/WebDev";
import Footer from "./components/footer";
import PostList from "./components/postList";
import ScrollToTop from "./components/util/scrollToTop";
import CssArtPost from "./components/postView/CssArtPost";
import NavbarPost from "./components/postView/NavbarPost";
import PolyShoePost from "./components/postView/PolyShoePost";
import SwedenPost from "./components/postView/SwedenPost";
import About from "./components/about";

function App() {
  return (
    <div className="App">
        <Router>
            <Fragment>
                <ScrollToTop />
                <Navigation />

                <Switch>
                    <Route exact path="/">
                        <HomePage />
                    </Route>
                    <Route exact path="/section/travel">
                        <Travel />
                    </Route>
                    <Route exact path="/section/web-development">
                        <WebDev />
                    </Route>
                    <Route exact path="/posts"
                        render={(props) => <PostList {...props} title={"Latest Posts"}/>}
                    />
                    <Route exact path="/posts/css-art">
                        <CssArtPost />
                    </Route>
                    <Route exact path="/posts/navbar-design">
                        <NavbarPost />
                    </Route>
                    <Route exact path="/posts/sneaker-art">
                        <PolyShoePost />
                    </Route>
                    <Route exact path="/posts/sweden-reflections">
                        <SwedenPost />
                    </Route>
                    <Route exact path="/author/chris-nunes"
                        render={(props) => <PostList {...props} title={"Posts by Chris Nunes"}/>}
                    />
                    <Route exact path="/about">
                        <About />
                    </Route>
                    <Route path="*">
                        <NotFound404 />
                    </Route>
                </Switch>

                <Footer />
            </Fragment>
        </Router>
    </div>
  );
}

export default App;
